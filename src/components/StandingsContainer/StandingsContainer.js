import React from 'react'
import styled from 'styled-components'

const Div = styled.div`
  flex-grow: 1;
`

const StandingsContainer = () => (
  <Div>StandingsContainer</Div>
)

export default StandingsContainer

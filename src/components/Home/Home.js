import React from 'react'
import styled from 'styled-components'
import Header from '../Header/Header'
import StandingsContainer from '../StandingsContainer/StandingsContainer'
import Footer from '../Footer/Footer'

const Wrapper = styled.section`
  padding: 4em;
  background: papayawhip;
  height: 100%;
  box-sizing: border-box;

  display: flex;
  flex-direction: column;
`

const Home = () => (
  <Wrapper>
    <Header />
    <StandingsContainer />
    <Footer />
  </Wrapper>
)

export default Home
